package com.search;

import java.util.LinkedList;

public interface ISearchProblem {
	public boolean goalTest(SearchNode node);
	public SearchNode initialState();
	public LinkedList<IProblemAction> getActions(SearchNode node);
	public SearchNode childNode(SearchNode node, IProblemAction action);
	public LinkedList<IProblemAction> buildSolution(SearchNode node);
}
