package com.search;

import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;

import com.search.util.AStarComparator;
import com.search.util.GreedyBestFirstComparator;
import com.search.util.SimpleFastPriorityQueue;
import com.search.util.UniformComparator;

public class Search {
	
	public static String searchTrace = "";
	public static String eol = System.getProperty("line.separator");
	
	public static boolean trace = false;
	public static long generatedNodes = 0;
	public static long exploredStates = 0;
	// uninformed search
	
	// ready //cutoff marking is empty LinkedList
	private static LinkedList<IProblemAction> recursiveDLS(ISearchProblem problem, SearchNode node, int limit) {
		if(problem.goalTest(node)) return problem.buildSolution(node);
		if(limit <= 0) return new LinkedList<IProblemAction>(); // cutoff
		
		if(trace) searchTrace += node + eol;
		boolean cutoff = false;
		LinkedList<IProblemAction> result = null;
		
		SearchNode child = null;
		IProblemAction action = null;
		LinkedList<IProblemAction> actions = problem.getActions(node);
		if(actions != null) {
			Iterator<IProblemAction> it = actions.iterator();
			while(it.hasNext()) {
				action = it.next();
				child = problem.childNode(node, action);
				
				generatedNodes++;
				if(trace) searchTrace += "	" + action + " -> " + child + eol;
				result = recursiveDLS(problem, child, limit - 1);
				
				// empty LinkedList, means cutoff
				if(result != null && result.isEmpty()) cutoff = true;
				else if(result != null) return result;
			}
		}
		
		if(cutoff) return new LinkedList<IProblemAction>();
		else return null;
	}

	// ready
	private static LinkedList<IProblemAction> depthLimited(ISearchProblem problem, int limit) {
		return recursiveDLS(problem, problem.initialState(), limit);
	}
	
	// ready //cutoff marking is empty LinkedList
	public static LinkedList<IProblemAction> iterativeDeepening(ISearchProblem problem) {
		generatedNodes = 0;
		exploredStates = 0;
		if(trace) searchTrace = "";
		LinkedList<IProblemAction> result = null;
		for(int depth = 0; ; depth++){
			result = depthLimited(problem, depth);
			if(result == null || !result.isEmpty()) return result; 
		}
	}
	
	// ready
	public static LinkedList<IProblemAction> breadthFirstSearch(ISearchProblem problem) {
		HashSet<IProblemState> explored = new HashSet<IProblemState>();
		LinkedList<SearchNode> frontier = new LinkedList<SearchNode>();
		
		generatedNodes = 0;
		exploredStates = 0;
		if(trace) searchTrace = "";
		
		SearchNode node = problem.initialState();
		frontier.offerLast(node);
		if(problem.goalTest(node)) return problem.buildSolution(node);
		while(true) {
			if(frontier.isEmpty()) return null;
			node = frontier.pollFirst();
			if(trace) searchTrace += node + eol;
			
			explored.add(node.state);
			exploredStates++;
			IProblemAction action = null;
			SearchNode child = null;
			LinkedList<IProblemAction> actions = problem.getActions(node);
			if(actions != null) {
				Iterator<IProblemAction> it = actions.iterator();
				while(it.hasNext()) {
					action = it.next();
					child = problem.childNode(node, action);
					
					generatedNodes++;
					if(trace) searchTrace += "	" + action + " -> " + child + eol;
					if(!explored.contains(child.state) && !frontier.contains(child)) {
						if(problem.goalTest(child)) {
							return problem.buildSolution(child);
						} else frontier.offerLast(child);
					}
				}
			}
		}
	}

	// informed search
	
	// ready
	private static LinkedList<IProblemAction> dijkstra(ISearchProblem problem, Comparator<SearchNode> comparator) {
		HashSet<IProblemState> explored = new HashSet<IProblemState>();
		SimpleFastPriorityQueue<SearchNode> frontier = new SimpleFastPriorityQueue<SearchNode>(comparator);
		
		generatedNodes = 0;
		exploredStates = 0;
		if(trace) searchTrace = "";
		
		SearchNode node = problem.initialState();
		frontier.offer(node);
		while(true) {
			if(frontier.isEmpty()) return null;
			node = frontier.poll();
			
			if(trace) searchTrace += node + eol;
			if(problem.goalTest(node)) return problem.buildSolution(node);
		
			explored.add(node.state);
			exploredStates++;
			
			IProblemAction action = null;
			SearchNode child = null;
			LinkedList<IProblemAction> actions = problem.getActions(node);
			if(actions != null) {
				Iterator<IProblemAction> it = actions.iterator();
				while(it.hasNext()) {
					action = it.next();
					child = problem.childNode(node, action);
					
					generatedNodes++;
					if(trace) searchTrace += "	" + action + " -> " + child + eol;
					if(!explored.contains(child.state)) {
						if(frontier.contains(child)) {
							frontier.decrease(child);
						} else {
							frontier.offer(child);
						}
					}
				}
			}
		}
	}
	
	// ready
	public static LinkedList<IProblemAction> uniformCost(ISearchProblem problem) {
		return dijkstra(problem, UniformComparator.getInstance());
	}
	
	//ready
	public static LinkedList<IProblemAction> greedyBestFirst(ISearchProblem problem) {
		return dijkstra(problem, GreedyBestFirstComparator.getInstance());
	}
	
	// ready
	public static LinkedList<IProblemAction> aStar(ISearchProblem problem) {
		return dijkstra(problem, AStarComparator.getInstance());
	}
	
	private static LinkedList<IProblemAction> recursiveBFS(ISearchProblem problem, SearchNode node, int f_limit) {
		if(problem.goalTest(node)) return problem.buildSolution(node);
		if(trace) searchTrace += node + eol;
		SimpleFastPriorityQueue<SearchNode> successors = new SimpleFastPriorityQueue<SearchNode>(AStarComparator.getInstance());
		LinkedList<IProblemAction> result = null;
		
		SearchNode child = null;
		IProblemAction action = null;
		LinkedList<IProblemAction> actions = problem.getActions(node);
		if(actions != null) {
			Iterator<IProblemAction> it = actions.iterator();
			while(it.hasNext()) {
				action = it.next();
				child = problem.childNode(node, action);
				child.heuristicCost = Math.max(child.heuristicCost + child.pathCost, node.heuristicCost + node.pathCost) - child.pathCost;
				generatedNodes++;
				if(trace) searchTrace += "	" + action + " -> " + child + eol;
				successors.offer(child);
			}
		}
		
		if(successors.isEmpty()) {
			node.heuristicCost = Integer.MAX_VALUE - node.pathCost;
			return null;
		}
		
		SearchNode best = null;
		SearchNode alternative = null;
		while(true) {
			best = successors.poll();
			if(best.pathCost + best.heuristicCost > f_limit) {
				node.heuristicCost = best.pathCost + best.heuristicCost - node.pathCost;
				return null;
			}
			alternative = successors.poll();
			
			result = recursiveBFS(problem, best, (alternative != null ? Math.min(f_limit, alternative.heuristicCost + alternative.pathCost) : f_limit));
			
			successors.offer(best);
			if(alternative != null) successors.offer(alternative);
			if(result != null) return result;
		}
	
	}

	// ready
	public static LinkedList<IProblemAction> RBFS(ISearchProblem problem) {
		generatedNodes = 0;
		exploredStates = 0;
		if(trace) searchTrace = "";
		return recursiveBFS(problem, problem.initialState(), Integer.MAX_VALUE);
	}
	
}