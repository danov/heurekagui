package com.search.util;

import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Queue;

import org.teneighty.heap.FibonacciHeap;
import org.teneighty.heap.Heap.Entry;

public class SimpleFastPriorityQueue<T> implements Queue<T> {
	HashMap<Integer,Entry<T, Integer>> keymap;
	FibonacciHeap<T, Integer> queue;
	Comparator<T> comparator;
	
	public SimpleFastPriorityQueue() {
		keymap = new HashMap<Integer,Entry<T, Integer>>();
		queue = new FibonacciHeap<T, Integer>();
	}
	
	public SimpleFastPriorityQueue(Comparator<T> comparator) {
		keymap = new HashMap<Integer,Entry<T, Integer>>();
		queue = new FibonacciHeap<T, Integer>(comparator);
		this.comparator = comparator;
	}

	@Override
	public int size() {
		return queue.getSize();
	}

	@Override
	public boolean isEmpty() {
		return queue.isEmpty();
	}

	@Override
	public boolean contains(Object o) {
		return keymap.containsKey(o.hashCode());
	}

	@Override
	public Iterator<T> iterator() {
		return queue.getKeys().iterator();
	}

	@Override
	public Object[] toArray() {
		return queue.getKeys().toArray();
	}

	@Override
	public <TT> TT[] toArray(TT[] a) {
		return queue.getKeys().toArray(a);
	}

	@Override
	public boolean remove(Object o) {
		Entry<T, Integer> entry = keymap.get(o.hashCode()); 
		try {
			keymap.remove(o.hashCode());
			queue.delete(entry);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean addAll(Collection<? extends T> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void clear() {
		keymap.clear();
		queue.clear();
	}

	@Override
	public boolean add(T e) {
		return offer(e);
	}

	@Override
	public boolean offer(T e) {
		try {
			Entry<T, Integer> entry = queue.insert(e, e.hashCode());
			keymap.put(e.hashCode(), entry);
		} catch(Exception ex) {
			return false;
		}
		return true;
	}

	@Override
	public T remove() {
		return poll();
	}

	@Override
	public T poll() {
		Entry<T, Integer> element;
		try {
			element = queue.extractMinimum();
			keymap.remove(element.getValue());
			return element.getKey();
		} catch(Exception e) {
			return null;
		}
	}

	@Override
	public T element() {
		return peek();
	}

	@Override
	public T peek() {
		Entry<T, Integer> element;
		try {
			element = queue.getMinimum();
			return element.getKey();
		} catch(Exception e) {
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public boolean decrease(T a) {
		Entry<T, Integer> element = keymap.get(a.hashCode());
		if((a instanceof Comparable && ((Comparable<T>) a).compareTo(element.getKey()) < 0) || 
					(comparator != null && comparator.compare(a, element.getKey()) < 0)) {
			try{
				queue.decreaseKey(element, a);
				return true;
			} catch(Exception ex) {
				return false;
			}
		} else return false;
	}
	
	@SuppressWarnings("unchecked")
	public boolean increase(T a) {
		Entry<T, Integer> element = keymap.get(a.hashCode());
		if((a instanceof Comparable && ((Comparable<T>) a).compareTo(element.getKey()) > 0) || 
					(comparator != null && comparator.compare(a, element.getKey()) > 0)) {
			try{
				this.remove(a);
				this.add(a);
				return true;
			} catch(Exception ex) {
				return false;
			}
		} else return false;
	}

}
