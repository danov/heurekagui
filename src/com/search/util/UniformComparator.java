package com.search.util;

import java.util.Comparator;

import com.search.SearchNode;

public class UniformComparator implements Comparator<SearchNode> {
	@Override
	public int compare(SearchNode a, SearchNode b) {
		int diff = (a.pathCost - b.pathCost);
		return diff;
	}
	
	private static UniformComparator instance = null;

	public static UniformComparator getInstance() {
		if(instance == null) instance = new UniformComparator();
		return instance;
	}
}
