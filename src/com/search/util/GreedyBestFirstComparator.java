package com.search.util;

import java.util.Comparator;

import com.search.SearchNode;

public class GreedyBestFirstComparator implements Comparator<SearchNode> {
	@Override
	public int compare(SearchNode a, SearchNode b) {
		int diff = (a.heuristicCost - b.heuristicCost);
		return diff;
	}
	
	private static GreedyBestFirstComparator instance = null;

	public static GreedyBestFirstComparator getInstance() {
		if(instance == null) instance = new GreedyBestFirstComparator();
		return instance;
	}
}
