package com.search;

public class SearchNode implements Comparable<SearchNode> {
	public IProblemState state;
	public SearchNode parent;
	public IProblemAction action;
	public int pathCost;
	public int heuristicCost;
	
	@Override
	public int compareTo(SearchNode o) {
		return pathCost - o.pathCost;
	}
	
	@Override
	public boolean equals(Object o) {
		if(o != null && o.getClass().equals(getClass())) {
			SearchNode temp = (SearchNode) o;
			return state.equals(temp.state);
		}
		else return false;
	}
	
	@Override
	public int hashCode() {
		return state.hashCode();
	}
	
	@Override
	public String toString() {
		return ("SearchNode(" + pathCost + "," + heuristicCost + "): " + state); 
	}
	
	@Override
	public Object clone() {
		SearchNode copy = new SearchNode();
		copy.state = state;
		copy.parent = parent;
		copy.action = action;
		copy.pathCost = pathCost;
		copy.heuristicCost = heuristicCost;
		return copy;
	}
}
