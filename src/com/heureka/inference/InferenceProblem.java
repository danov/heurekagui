package com.heureka.inference;

import java.io.BufferedReader;
import java.io.StringReader;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;

import com.search.IProblemAction;
import com.search.IProblemState;
import com.search.ISearchProblem;
import com.search.SearchNode;

public class InferenceProblem implements ISearchProblem {

	private HashSet<InferenceState> kbPool;
	
	private boolean ancestor = false;
	private boolean refutation = true;
	private InferenceState goalState;
	private InferenceState initState;
	
	@Override
	public boolean goalTest(SearchNode node) {
		InferenceState infs = (InferenceState) node.state;
		return infs.equals(goalState);
	}

	@Override
	public SearchNode initialState() {
		initState = new InferenceState(new String[] {"this.is.special.node"}, new String[] {"this.is.special.node"});
		initState.special = true;
		
		SearchNode nsn = new SearchNode();
		nsn.parent = null;
		nsn.pathCost = 0;
		nsn.state = initState;
		nsn.action = null;
		nsn.heuristicCost = calculateDistance(nsn.state, goalState);
		
		return nsn;
	}

	@Override
	public LinkedList<IProblemAction> getActions(SearchNode node) {
		LinkedList<IProblemAction> result = new LinkedList<IProblemAction>(); 
		InferenceState clause = (InferenceState) node.state;
		
		if(isAncestor()) {
			SearchNode temp = node;
			while(temp.parent != null) {
				temp = temp.parent;
				InferenceState tempclause = (InferenceState) temp.state;
				if(!clause.special) {
					result.addAll(clause.resolvable(tempclause));
				}
			}
		}
		
		Iterator<InferenceState> it = kbPool.iterator();
		while(it.hasNext()) {
			InferenceState temp = it.next();
			if(temp.equals(clause)) continue;
			
			if(clause.special) {
				if(isRefutation()) {
					if(temp.negatedGoal) result.add(new InferenceAction(temp, 1, "", false));
				} else {
					result.add(new InferenceAction(temp, 1, "", false));
				}
			} else {
				result.addAll(clause.resolvable(temp));
			}
		}
		return result;
	}

	@Override
	public SearchNode childNode(SearchNode node, IProblemAction action) {
		if(node == null || action == null) return null;
		InferenceAction act = (InferenceAction) action;
		InferenceState state = (InferenceState) node.state;
		
		SearchNode nsn = new SearchNode();
		nsn.parent = node;
		nsn.pathCost = node.pathCost + ((InferenceAction)action).stepCost;
		nsn.action = action;
		
		if(state.special) {
			nsn.state = act.clause;
		} else {
			nsn.state = state.resolveWithOn(act.clause, act.literal, act.neg);
		}
		
		nsn.heuristicCost = calculateDistance(nsn.state, goalState);
		return nsn;
	}

	@Override
	public LinkedList<IProblemAction> buildSolution(SearchNode node) {
		LinkedList<IProblemAction> arr = new LinkedList<IProblemAction>();
		SearchNode temp = node;
		while(temp.action != null) {
			arr.add(temp.action);
			temp = temp.parent;
		}
		Collections.reverse(arr);
		return arr;
	}

	public void parseKnowledgeBase(String kb) {
		kbPool = new HashSet<InferenceState>();
		BufferedReader br;
		try {
			br = new BufferedReader(new StringReader(kb));
			
			String line = br.readLine();
			while (line != null)
			{
				if(line.length() > 0) {
					String[] split = line.split("<|if");
					String[] pos = (split.length > 0 && split[0].length() > 0 ? split[0].split("\\s+") : new String[]{});
					String[] neg = (split.length > 1 && split[1].length() > 0 ? split[1].split("\\s+") : new String[]{});
					InferenceState is = new InferenceState(pos, neg);
					kbPool.add(is);
				}
				line = br.readLine();
			}
			br.close();
		} catch(Exception e) {
			
		}
	}

	public void setGoal(String str) {
		if(str.length() > 0) {
			String[] split = str.split("<|if");
			String[] pos = (split.length > 0 && split[0].length() > 0 ? split[0].split("\\s+") : new String[]{});
			String[] neg = (split.length > 1 && split[1].length() > 0 ? split[1].split("\\s+") : new String[]{});
			InferenceState is = new InferenceState(pos, neg);
			if(isRefutation()) {
				HashSet<InferenceState> negGoal = is.negate();
				
				Iterator<InferenceState> it = negGoal.iterator();
				while(it.hasNext()) {
					InferenceState temp = it.next();
					temp.negatedGoal = true;
				}
				
				kbPool.addAll(negGoal);
				goalState = new InferenceState(new String[] {""}, new String[] {""});
			} else {
				goalState = is;
			}
		}
	}

	public boolean isAncestor() {
		return ancestor;
	}

	public void setAncestor(boolean ancestor) {
		this.ancestor = ancestor;
	}

	public boolean isRefutation() {
		return refutation;
	}

	public void setRefutation(boolean refutation) {
		this.refutation = refutation;
	}
	
	private int calculateDistance(IProblemState state, InferenceState goal) {
		InferenceState ifs = (InferenceState) state;
		InferenceState ifsg = (InferenceState) goal;
		if(state == null || goal == null) return 0;
		
		if(ifs.special) return 0;
		else {
			return Math.abs(ifs.numLiterals() - ifsg.numLiterals());
		}
	}

}
