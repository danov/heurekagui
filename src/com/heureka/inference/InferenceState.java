package com.heureka.inference;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;

import com.search.IProblemState;

public class InferenceState implements IProblemState {
	
	private HashSet<String> posLiterals;
	private HashSet<String> negLiterals;
	
	public boolean special = false;
	public boolean negatedGoal = false;
	
	@SuppressWarnings("unchecked")
	private InferenceState(HashSet<String> plit, HashSet<String> nlit) {
		posLiterals = (HashSet<String>) plit.clone();
		negLiterals = (HashSet<String>) nlit.clone();
	}
	
	@SuppressWarnings("unchecked")
	private InferenceState(InferenceState clause) {
		posLiterals = (HashSet<String>) clause.posLiterals.clone();
		negLiterals = (HashSet<String>) clause.negLiterals.clone();
		special = clause.special;
		negatedGoal = clause.negatedGoal;
	}
	
	public InferenceState(String[] pos, String[] neg) {
		posLiterals = new HashSet<String>();
		negLiterals = new HashSet<String>();
		
		for(int i = 0; i < pos.length; i++) {
			if(pos[i].length() > 0) posLiterals.add(pos[i]);
		}
		
		for(int j = 0; j < neg.length; j++) {
			if(neg[j].length() > 0) negLiterals.add(neg[j]);
		}
	}
	
	public int numLiterals() {
		return posLiterals.size() + negLiterals.size();
	}
	
	@Override
	public boolean equals(Object o) {
		if(o != null && o.getClass().equals(getClass())) return posLiterals.equals(((InferenceState) o).posLiterals) && negLiterals.equals(((InferenceState) o).negLiterals);
		else return false;
	}
	
	@Override
	public int hashCode() {
		return 13 * posLiterals.hashCode() + negLiterals.hashCode();
	}
	
	@Override
	public String toString() {
		Iterator<String> it = posLiterals.iterator();
		Iterator<String> jt = negLiterals.iterator();
		
		String result = "{";
		if(it.hasNext()) result += it.next();
		while(it.hasNext()) {
			result += ", " + it.next();
		}
		
		if(jt.hasNext()) result += (posLiterals.size() > 0 ? ", " : "") + "¬" + jt.next();
		while(jt.hasNext()) {
			result += ", ¬" + jt.next();
		}
		result += "}";
		
		return result; 
	}
	
	@Override
	public Object clone() {
		return new InferenceState(this);
	}

	public HashSet<InferenceState> negate() {
		HashSet<InferenceState> result = new HashSet<InferenceState>();
		
		Iterator<String> it = posLiterals.iterator();
		Iterator<String> jt = negLiterals.iterator();
		
		while(it.hasNext()) {
			result.add(new InferenceState(new String[]{ }, new String[]{ it.next() }));
		}
		
		while(jt.hasNext()) {
			result.add(new InferenceState(new String[]{ jt.next() }, new String[]{ }));
		}
		
		return result;
	}
	
	public LinkedList<InferenceAction> resolvable(InferenceState clause) {
		LinkedList<InferenceAction> result = new LinkedList<InferenceAction>();
		
		Iterator<String> it = clause.posLiterals.iterator();
		Iterator<String> jt = clause.negLiterals.iterator();
		
		while(it.hasNext()) {
			String temp = it.next();
			if(negLiterals.contains(temp)) {
				result.add(new InferenceAction(new InferenceState(clause), 1, new String(temp), false));
			}
		}
		
		while(jt.hasNext()) {
			String temp = jt.next();
			if(posLiterals.contains(temp)) {
				result.add(new InferenceAction(new InferenceState(clause), 1, new String(temp), true));
			}
		}
		
		return result;
	}

	@SuppressWarnings("unchecked")
	public InferenceState resolveWithOn(InferenceState clause, String literal, boolean negated) {
		InferenceState resolvent = null;
		InferenceState cloned = (InferenceState) clause.clone();
		
		HashSet<String> plit = new HashSet<String>();
		HashSet<String> nlit = new HashSet<String>();
		
		plit = (HashSet<String>) posLiterals.clone();
		nlit = (HashSet<String>) negLiterals.clone();
		
		if(negated) {
			plit.remove(literal);
			plit.addAll(cloned.posLiterals);
			cloned.negLiterals.remove(literal);
			nlit.addAll(cloned.negLiterals);
		} else {
			nlit.remove(literal);
			nlit.addAll(cloned.negLiterals);
			cloned.posLiterals.remove(literal);
			plit.addAll(cloned.posLiterals);
		}
		
		resolvent = new InferenceState(plit, nlit);
		return resolvent;
	}
}
