package com.heureka.inference;

import com.search.IProblemAction;

public class InferenceAction implements IProblemAction {
	public InferenceState clause;
	public int stepCost;
	public String literal;
	public boolean neg;
	
	public InferenceAction(InferenceState state, int cost, String lit, boolean negated) {
		clause = state;
		stepCost = cost;
		literal = lit;
		neg = negated;
	}
	
	@Override
	public boolean equals(Object o) {
		if(o != null && o.getClass().equals(getClass())) {
			InferenceAction temp = (InferenceAction) o;
			return clause.equals(temp.clause) && stepCost == temp.stepCost && literal.equals(temp.literal) && neg == temp.neg;
		} else return false;
	}
	
	@Override
	public int hashCode() {
		return clause.hashCode()<<16 | literal.hashCode()<<8 | stepCost<<4 | (neg ? 0 : 1);
	}
	
	@Override
	public String toString() {
		return "resolve with " + clause + " on {" + (neg ? "¬" : "") + literal + "}"; 
	}
}
