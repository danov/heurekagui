package com.heureka.map;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Set;

import com.search.IProblemAction;
import com.search.IProblemState;
import com.search.ISearchProblem;
import com.search.SearchNode;

public class MapProblem implements ISearchProblem {

	private HashMap<IProblemState,LinkedList<IProblemAction>> data; 
	private IProblemState init;
	private IProblemState goal;
	
	@Override
	public SearchNode initialState() {
		SearchNode nsn = new SearchNode();
		nsn.parent = null;
		nsn.pathCost = 0;
		nsn.state = init;
		nsn.action = null;
		nsn.heuristicCost = calculateDistance(nsn.state, goal);
		return nsn;
	}

	@Override
	public boolean goalTest(SearchNode node) {
		return goal.equals(node.state);
	}

	@Override
	public LinkedList<IProblemAction> getActions(SearchNode node) {
		if(data.containsKey(node.state)) return data.get(node.state);
		else return null;
	}

	@Override
	public SearchNode childNode(SearchNode node, IProblemAction action) {
		if(node == null || action == null) return null;
		SearchNode nsn = new SearchNode();
		nsn.parent = node;
		nsn.pathCost = node.pathCost + ((MapAction)action).stepCost;
		nsn.state = ((MapAction)action).target;
		nsn.action = action;
		nsn.heuristicCost = calculateDistance(nsn.state, goal);
		return nsn;
	}
	
	@Override
	public LinkedList<IProblemAction> buildSolution(SearchNode node) {
		LinkedList<IProblemAction> arr = new LinkedList<IProblemAction>();
		SearchNode temp = node;
		while(temp.action != null) {
			arr.add(temp.action);
			temp = temp.parent;
		}
		Collections.reverse(arr);
		return arr;
	}

	public boolean loadData(String file) {
		data = new HashMap<IProblemState,LinkedList<IProblemAction>>();
		BufferedReader br;
		try {
			br = new BufferedReader(new FileReader(file));

			String line = br.readLine();
			String[] parts;
			while (line != null)
			{
				parts = line.split(" ");
				
				if(parts.length < 5) {
					line = br.readLine();
					continue;
				}
				MapState src = new MapState();
				src.x = Integer.parseInt(parts[0]);
				src.y = Integer.parseInt(parts[1]);
				
				MapState dest = new MapState();
				dest.x = Integer.parseInt(parts[3]);
				dest.y = Integer.parseInt(parts[4]);
				
				if(!data.containsKey(src)) data.put(src, new LinkedList<IProblemAction>());
				if(!data.containsKey(dest)) data.put(dest, new LinkedList<IProblemAction>());
				data.get(src).add(new MapAction(parts[2], dest, calculateDistance(src, dest)));
				line = br.readLine();
			}
			br.close();
			setInit(0, 0);
			setGoal(200, 200);
		} catch(Exception e) {
			
		}
		
		if(data.size() > 0) return true;
		else return false;
	}
	
	public boolean getLoaded() {
		return data != null;
	}

	public HashMap<IProblemState,LinkedList<IProblemAction>> getProblem() {
		return data;
	}

	public void setInit(int x, int y) {
		MapState temp = new MapState();
		((MapState)temp).x = x; ((MapState)temp).y = y;
		int bestDistance = Integer.MAX_VALUE;
		int tempDistance;
		
		if(data == null) return;
		Set<IProblemState> kset = data.keySet();
		Iterator<IProblemState> it = kset.iterator();
		MapState ms;
		
		while(it.hasNext()) {
			ms = (MapState) it.next();
			tempDistance = calculateDistance(temp, ms);
			if(tempDistance < bestDistance) {
				init = ms;
				bestDistance = tempDistance; 
			}
		}
	}
	
	public MapState getInit() {
		return (MapState) init;
	}
	
	public void setGoal(int x, int y) {
		MapState temp = new MapState();
		((MapState)temp).x = x; ((MapState)temp).y = y;
		int bestDistance = Integer.MAX_VALUE;
		int tempDistance;
		
		if(data == null) return;
		Set<IProblemState> kset = data.keySet();
		Iterator<IProblemState> it = kset.iterator();
		MapState ms;
		
		while(it.hasNext()) {
			ms = (MapState) it.next();
			tempDistance = calculateDistance(temp, ms);
			if(tempDistance < bestDistance) {
				goal = ms;
				bestDistance = tempDistance; 
			}
		}
	}

	public MapState getGoal() {
		return (MapState) goal;
	}

	private int calculateDistance(IProblemState a, IProblemState g) {
		double xdiff = Math.abs(((MapState)a).x - ((MapState)g).x);
		double ydiff = Math.abs(((MapState)a).y - ((MapState)g).y);
		double distance = Math.sqrt(xdiff*xdiff + ydiff*ydiff);
		return (int) distance;
	}

}
