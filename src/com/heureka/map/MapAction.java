package com.heureka.map;

import com.search.IProblemAction;

public class MapAction implements IProblemAction {
	public String name;
	public MapState target;
	public int stepCost;
	
	public MapAction(String name, MapState destination, int stepCost) {
		this.name = name;
		this.target = destination;
		this.stepCost = stepCost;
	}
	
	@Override
	public boolean equals(Object o) {
		if(o != null && o.getClass().equals(getClass())) {
			MapAction temp = (MapAction) o;
			return name.equals(temp.name) && target.equals(temp.target) && stepCost == temp.stepCost;
		}
		else return false;
	}
	
	@Override
	public int hashCode() {
		return name.hashCode()<<8 | target.hashCode()<<4 | stepCost;
	}
	
	@Override
	public String toString() {
		return ("Action {" + name + ", " + stepCost + "} -> " + target); 
	}
}
