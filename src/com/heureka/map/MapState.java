package com.heureka.map;

import com.search.IProblemState;

public class MapState implements IProblemState {

	public int x;
	public int y;
	
	@Override
	public boolean equals(Object o) {
		if(o != null && o.getClass().equals(getClass())) return x == ((MapState)o).x && y == ((MapState)o).y;
		else return false;
	}
	
	@Override
	public int hashCode() {
		return (x<<16 | y);
	}
	
	@Override
	public String toString() {
		return ("State {" + Integer.toString(x) + ", " + Integer.toString(y) + "}"); 
	}

}
