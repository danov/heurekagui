package com.heureka;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Iterator;
import java.util.LinkedList;

import com.heureka.inference.InferenceAction;
import com.heureka.inference.InferenceProblem;
import com.heureka.inference.InferenceState;
import com.search.IProblemAction;
import com.search.Search;

public class HeurekaInference {

	private InferenceProblem infProblem;
	private String knowledgeBase = "";
	
	public HeurekaInference() {
		infProblem = new InferenceProblem();
	}
	
	public String loadKnowledgeBase(String path) {
		BufferedReader br;
		try {
			br = new BufferedReader(new FileReader(path));
			String eol = System.getProperty("line.separator");
			knowledgeBase = "";
			String line = br.readLine();
			while (line != null)
			{
				knowledgeBase += line + eol;
				line = br.readLine();
			}
			br.close();
		} catch(Exception e) {
			
		}
		
		return knowledgeBase;
	}
	
	public void parseKnowledgeBase(String kb) {
		infProblem.parseKnowledgeBase(kb);
	}
	
	public String prove(String prop, boolean ancestor, boolean refutation, boolean rbfs) {
		String result = "";
		String eol = System.getProperty("line.separator");
		infProblem.setAncestor(ancestor);
		infProblem.setRefutation(refutation);
		infProblem.setGoal(prop);

		LinkedList<IProblemAction> solution;
		long millisec = System.currentTimeMillis();
		if(rbfs){
			solution = Search.RBFS(infProblem);
		} else {
			solution = Search.aStar(infProblem);
		}
		millisec = System.currentTimeMillis() - millisec;
		
		if(solution != null) {
			result = "";
			InferenceState ms = (InferenceState) infProblem.initialState().state;
			Iterator<IProblemAction> it = solution.iterator();
			if(solution.size() == 1) result = "\"" + prop + "\" exists in knowledgebase";
			while(it.hasNext()) {
				InferenceAction ia = (InferenceAction) it.next();
				if(ms.special) {
					ms = ia.clause;
				} else {
					result += ms + " -> " + ia + " -> ";
					ms = ms.resolveWithOn(ia.clause, ia.literal, ia.neg);
					result += ms + eol;
				}
			}
		} else result = "no solution found";
		
		String text = "Time for execution: " + Long.toString(millisec) + " ms" + eol;
		text += "Explored states: " + Long.toString(Search.exploredStates) + eol;
		text += "Generated nodes: " + Long.toString(Search.generatedNodes) + eol + eol;
		text += "Solution action sequence: " + eol + result + eol + eol;
		text += "Expanding sequence (debug): " + eol;
		text += Search.searchTrace + eol;
		
		return text;
	}
	
}
