package com.heureka;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Set;

import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;

import com.heureka.map.MapAction;
import com.heureka.map.MapProblem;
import com.heureka.map.MapState;
import com.search.IProblemAction;
import com.search.IProblemState;
import com.search.Search;

public class HeurekaMap {
	
	private MapProblem mapProblem;
	private int algorithm;
	private LinkedList<IProblemAction> result;
	
	public HeurekaMap() {
		mapProblem = new MapProblem();
		algorithm = -1;
	}
	
	public boolean loadMapProblem(String path) {
		return mapProblem.loadData(path);
	}
	
	public void solve(int alg) {
		algorithm = alg;
		if(!mapProblem.getLoaded()) return;
		switch(algorithm) {
			case 0:
				result = Search.breadthFirstSearch(mapProblem);
				break;
			case 1:
				result = Search.iterativeDeepening(mapProblem);
				break;
			case 2:
				result = Search.uniformCost(mapProblem);
				break;
			case 3:
				result = Search.greedyBestFirst(mapProblem);
				break;
			case 4:
				result = Search.aStar(mapProblem);
				break;
			case 5:
				result = Search.RBFS(mapProblem);
				break;
			default:
				algorithm = -1;
		}
	}

	public void setInit(int x, int y) {
		mapProblem.setInit(x, y);
	}
	
	public void setGoal(int x, int y) {
		mapProblem.setGoal(x, y);
	}
	
	public Point getInit() {
		MapState ms = mapProblem.getInit();
		if(ms != null) return new Point(ms.x, ms.y);
		else return null;
	}
	
	public Point getGoal() {
		MapState ms = mapProblem.getGoal();
		if(ms != null) return new Point(ms.x, ms.y);
		else return null;
	}

	public LinkedList<Point> getNodes() {
		LinkedList<Point> nodes = new LinkedList<Point>();
		if(!mapProblem.getLoaded()) return nodes;
		HashMap<IProblemState, LinkedList<IProblemAction>> lhm = mapProblem.getProblem();
		Set<IProblemState> kset = lhm.keySet();
		Iterator<IProblemState> it = kset.iterator();
		MapState ms;
		while(it.hasNext()) {
			ms = (MapState) it.next();
			Point point = new Point(ms.x, ms.y);
			nodes.add(point);
		}
		return nodes;
	}
	
	public LinkedList<Rectangle> getEdges() { 
		LinkedList<Rectangle> edges = new LinkedList<Rectangle>();
		if(!mapProblem.getLoaded()) return edges;
		HashMap<IProblemState, LinkedList<IProblemAction>> lhm = mapProblem.getProblem();
		Set<IProblemState> kset = lhm.keySet();
		Iterator<IProblemState> it = kset.iterator();
		MapState ms;
		while(it.hasNext()) {
			ms = (MapState) it.next();
			LinkedList<IProblemAction> actions = lhm.get(ms);
			Iterator<IProblemAction> jt = actions.iterator();
			MapAction ma;
			while(jt.hasNext()) {
				ma = (MapAction) jt.next();
				Rectangle rect = new Rectangle(ms.x, ms.y, ma.target.x, ma.target.y);
				edges.add(rect);
			}
		}
		return edges;
	}
	
	public String getSolution() {
		String solution = "";
		String eol = System.getProperty("line.separator");
		if(result != null) {
			MapState ms = (MapState) mapProblem.initialState().state;
			Iterator<IProblemAction> it = result.iterator();
			while(it.hasNext()) {
				MapAction ma = (MapAction) it.next();
				solution += ms + " -> " + ma + eol;
				ms = ma.target;
			}
		}
		return solution;
	}
	
	public LinkedList<Rectangle> getSolutionEdges() {
		LinkedList<Rectangle> solution = new LinkedList<Rectangle>();
		Rectangle rect;
		if(result != null) {
			MapState ms = (MapState) mapProblem.initialState().state;
			Iterator<IProblemAction> it = result.iterator();
			while(it.hasNext()) {
				MapAction ma = (MapAction) it.next();
				rect = new Rectangle(ms.x, ms.y, ma.target.x, ma.target.y);
				solution.add(rect);
				ms = ma.target;
			}
		}
		return solution;
	}
}
