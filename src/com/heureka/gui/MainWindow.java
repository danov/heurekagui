package com.heureka.gui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;

public class MainWindow {

	protected Shell shlHeureka;
	private TabItem tbtmTheoremProver;
	private TabFolder tabFolder;
	private TabItem tbtmMap;
	private Composite routePlanning;
	private Composite inferenceEngine;
	
	/**
	 * Launch the application.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			MainWindow window = new MainWindow();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 */
	public void open() {
		Display display = Display.getDefault();
		
		createContents();
		
		shlHeureka.open();
		shlHeureka.layout();
		while (!shlHeureka.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shlHeureka = new Shell();
		shlHeureka.setMinimumSize(new Point(800, 800));
		shlHeureka.setSize(800, 800);
		shlHeureka.setText("Heureka");
		
		tabFolder = new TabFolder(shlHeureka, SWT.NONE);
		tabFolder.setSelection(1);
		tabFolder.setEnabled(true);
		tabFolder.setBounds(10, 10, 764, 742);

		tbtmMap = new TabItem(tabFolder, SWT.NONE);
		tbtmMap.setText("Route Planning");

		routePlanning = new RoutePlanningComposite(tabFolder, SWT.NONE);
		tbtmMap.setControl(routePlanning);

		tbtmTheoremProver = new TabItem(tabFolder, SWT.NONE);
		tbtmTheoremProver.setText("Inference Engine");

		inferenceEngine = new InferenceEngineComposite(tabFolder, SWT.NONE);
		tbtmTheoremProver.setControl(inferenceEngine);

	}
	
}
