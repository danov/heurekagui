package com.heureka.gui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.heureka.HeurekaInference;

public class InferenceEngineComposite extends Composite {

	private Composite inferenceEngine;
	private Label lblBrowseKB;
	
	private Group grpResolution;
	private Group grpAlgorithm;
	private Group grpProof;
	
	private Text txtCmd;
	private Text txtKnowledgeBase;
	private Text txtProofOutput;
	
	private Button btnRbfs;
	private Button btnA;
	private Button btnLoadKB;
	private Button btnProve;
	private Button btnDirectProof;
	private Button btnRefutationProof;
	private Button btnInputResolution;
	private Button btnAncestorResolution;
	
	private HeurekaInference heu;
	private boolean refutation = false;
	private boolean ancestor = false;
	private boolean rbfs = false;
	
	private Thread trd;
	
	public InferenceEngineComposite(Composite parent, int style) {
		super(parent, style);
		addDisposeListener(new DisposeListener() {
			@SuppressWarnings("deprecation")
			public void widgetDisposed(DisposeEvent arg0) {
				if(trd != null && trd.isAlive()) {
					trd.stop();
				}
			}
		});
		inferenceEngine = this;
		
		heu = new HeurekaInference();
		createLabels();
		createButtons();
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}
	
	protected void enableAll() {
		txtCmd.setEnabled(true);
		txtKnowledgeBase.setEnabled(true);
		
		btnRbfs.setEnabled(true);
		btnA.setEnabled(true);
		btnLoadKB.setEnabled(true);
		btnProve.setEnabled(true);
		btnDirectProof.setEnabled(true);
		btnRefutationProof.setEnabled(true);
		btnInputResolution.setEnabled(true);
		btnAncestorResolution.setEnabled(true);
	}
	
	protected void disableAll() {
		txtCmd.setEnabled(false);
		txtKnowledgeBase.setEnabled(false);
		
		btnRbfs.setEnabled(false);
		btnA.setEnabled(false);
		btnLoadKB.setEnabled(false);
		btnProve.setEnabled(false);
		btnDirectProof.setEnabled(false);
		btnRefutationProof.setEnabled(false);
		btnInputResolution.setEnabled(false);
		btnAncestorResolution.setEnabled(false);
	}
	
	protected void createLabels() {
		txtKnowledgeBase = new Text(inferenceEngine, SWT.BORDER | SWT.V_SCROLL | SWT.MULTI);
		txtKnowledgeBase.setBounds(10, 10, 365, 631);
		
		txtProofOutput = new Text(inferenceEngine, SWT.BORDER | SWT.V_SCROLL | SWT.MULTI);
		txtProofOutput.setEditable(false);
		txtProofOutput.setBounds(381, 10, 365, 631);
		
		lblBrowseKB = new Label(inferenceEngine, SWT.NONE);
		lblBrowseKB.setBounds(10, 684, 239, 15);
		lblBrowseKB.setText("Browse...");
	}
	
	protected void createButtons() {
		
		grpResolution = new Group(inferenceEngine, SWT.NONE);
		grpResolution.setText("Resolution");
		grpResolution.setBounds(442, 647, 143, 57);
		
		grpProof = new Group(inferenceEngine, SWT.NONE);
		grpProof.setText("Proof");
		grpProof.setBounds(591, 647, 155, 57);
		
		btnInputResolution = new Button(grpResolution, SWT.RADIO);
		btnInputResolution.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				ancestor = btnAncestorResolution.getSelection();
			}
		});
		btnInputResolution.setSelection(true);
		btnInputResolution.setBounds(10, 31, 49, 16);
		btnInputResolution.setText("Input");
		
		btnAncestorResolution = new Button(grpResolution, SWT.RADIO);
		btnAncestorResolution.setBounds(65, 31, 75, 16);
		btnAncestorResolution.setText("Ancestor");
		btnAncestorResolution.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				ancestor = btnAncestorResolution.getSelection();
			}
		});
		
		btnDirectProof = new Button(grpProof, SWT.RADIO);
		btnDirectProof.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				refutation = btnRefutationProof.getSelection();
			}
		});
		btnDirectProof.setSelection(true);
		btnDirectProof.setBounds(10, 31, 52, 16);
		btnDirectProof.setText("Direct");
		
		btnRefutationProof = new Button(grpProof, SWT.RADIO);
		btnRefutationProof.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				refutation = btnRefutationProof.getSelection();
			}
		});
		btnRefutationProof.setBounds(69, 31, 76, 16);
		btnRefutationProof.setText("Refutation");
		
		btnLoadKB = new Button(inferenceEngine, SWT.NONE);
		btnLoadKB.setBounds(255, 679, 75, 25);
		btnLoadKB.setText("Load KB");
		btnLoadKB.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				FileDialog fd = new FileDialog(inferenceEngine.getShell(), SWT.NULL);
				fd.setFilterExtensions(new String[] {"*.txt"});
				String path = fd.open();
				if(path == null) return;
				String kb = heu.loadKnowledgeBase(path);
				txtKnowledgeBase.setText(kb);
				lblBrowseKB.setText(path);
			}
		});
		
		btnProve = new Button(inferenceEngine, SWT.NONE);
		btnProve.setBounds(255, 648, 75, 25);
		btnProve.setText("Prove!");
		btnProve.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				disableAll();
				trd = new Thread() {
						HeurekaInference p = heu;
						String cmd = txtCmd.getText();
						String kb = txtKnowledgeBase.getText();
	            	   public void run() {
	            		 p.parseKnowledgeBase(kb);
	                	 final String result = p.prove(cmd, ancestor, refutation, rbfs);
	                	 Display.getDefault().syncExec(new Runnable() {
	                		    public void run() {
	                		    	enableAll();
	                		    	txtProofOutput.setText(result);
	                		    }
	                		});
	                 }
	               };
	            trd.start();
			}
		});
		txtCmd = new Text(this, SWT.BORDER);
		txtCmd.setBounds(10, 650, 239, 21);
		
		grpAlgorithm = new Group(this, SWT.NONE);
		grpAlgorithm.setText("Algorithm");
		grpAlgorithm.setBounds(336, 647, 100, 57);
		
		btnA = new Button(grpAlgorithm, SWT.RADIO);
		btnA.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				rbfs = btnRbfs.getSelection();
			}
		});
		btnA.setSelection(true);
		btnA.setBounds(10, 31, 34, 16);
		btnA.setText("A*");
		
		btnRbfs = new Button(grpAlgorithm, SWT.RADIO);
		btnRbfs.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				rbfs = btnRbfs.getSelection();
			}
		});
		btnRbfs.setBounds(50, 31, 47, 16);
		btnRbfs.setText("RBFS");
	}
}
