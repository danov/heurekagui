package com.heureka.gui;

import java.util.Iterator;
import java.util.LinkedList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.ScrollBar;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;

import com.heureka.HeurekaMap;
import com.search.Search;

public class RoutePlanningComposite extends Composite {

	protected HeurekaMap heu;
	protected Image mapImage;
	protected Image origImage;
	protected Point origin;
	protected int coordScale = 10;
	protected int initX = 0;
	protected int initY = 0;
	protected int goalX = 200;
	protected int goalY = 200;
	protected int xOffset = 0;
	protected int yOffset = 0;
	protected boolean reverseXY = false;
	protected boolean mirrorX = false;
	protected boolean mirrorY = false;
	protected int algorithm = -1;
	
	private MessageBox alert;
	private Label lblCoordinatesScale;
	private Label lblPathToFile;
	private Label lblInitialState;
	private Label lblGoalState;
	private Label lblBrowseImage;
	private Label lblChooseAlgorithm;
	private Label lblInitX;
	private Label lblInitY;
	private Label lblGoalX;
	private Label lblGoalY;
	private Label lblOffset;
	
	private Composite routePlanning;
	private Canvas canvas;
	
	private ScrollBar hBar;
	private ScrollBar vBar;
	
	private Text txtOutput;
	
	private Spinner scaleSpinner;
	private Spinner spinnerInitY;
	private Spinner spinnerInitX;
	private Spinner spinnerGoalX;
	private Spinner spinnerGoalY;
	private Spinner spinnerOffsetY;
	private Spinner spinnerOffsetX;
	
	private Combo combo;
	private Button btnLoadData;
	private Button btnLoadImage;
	private Button btnReverseXY;
	private Button btnMirrorX;
	private Button btnMirrorY;
	
	private Thread trd;
	private Control focused;
	
	public RoutePlanningComposite(Composite parent, int style) {
		super(parent, style);
		addDisposeListener(new DisposeListener() {
			@SuppressWarnings("deprecation")
			public void widgetDisposed(DisposeEvent arg0) {
				if(trd != null && trd.isAlive()) {
					trd.stop();
				}
			}
		});
		routePlanning = this;
		
		heu = new HeurekaMap();
		mapImage = new Image(Display.getDefault(), 1, 1);
		origImage = new Image(Display.getDefault(), 1, 1);
		origin = new Point(0, 0);
		
		createLabels();
		createSpinners();
		createButtons();
		createCanvas();
	}

	private void createCanvas() {
		
		alert = new MessageBox(routePlanning.getShell());
		
		canvas = new Canvas(routePlanning, SWT.H_SCROLL | SWT.V_SCROLL);
		canvas.setBounds(10, 10, 736, 540);
		canvas.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				Point pnt = retranslateCoordinates(e.x - origin.x, e.y - origin.y);
				if(e.button == 1) {
					initX = pnt.x;
					initY = pnt.y;
					spinnerInitX.setSelection(pnt.x);
					spinnerInitY.setSelection(pnt.y);
					heu.setInit(initX, initY);
					solveIt();
				}
				
				if(e.button == 3) {
					goalX = pnt.x;
					goalY = pnt.y;
					spinnerGoalX.setSelection(pnt.x);
					spinnerGoalY.setSelection(pnt.y);
					heu.setGoal(goalX, goalY);
					solveIt();
				}
			}
		});
		
		hBar = canvas.getHorizontalBar();
	    hBar.addListener(SWT.Selection, new Listener() {
	      public void handleEvent(Event e) {
	        int hSelection = hBar.getSelection();
	        int destX = -hSelection - origin.x;
	        Rectangle rect = mapImage.getBounds();
	        canvas.scroll(destX, 0, 0, 0, rect.width, rect.height, false);
	        origin.x = -hSelection;
	      }
	    });
	    
	    vBar = canvas.getVerticalBar();
	    vBar.addListener(SWT.Selection, new Listener() {
	      public void handleEvent(Event e) {
	        int vSelection = vBar.getSelection();
	        int destY = -vSelection - origin.y;
	        Rectangle rect = mapImage.getBounds();
	        canvas.scroll(0, destY, 0, 0, rect.width, rect.height, false);
	        origin.y = -vSelection;
	      }
	    });
	    
	    canvas.addPaintListener(new PaintListener() {
			public void paintControl(PaintEvent arg0) {
				GC gc = arg0.gc;
		        gc.drawImage(mapImage, origin.x, origin.y);
		        Rectangle rect = mapImage.getBounds();
		        Rectangle client = canvas.getClientArea();
		        int marginWidth = client.width - rect.width;
		        if (marginWidth > 0) {
		          gc.fillRectangle(rect.width, 0, marginWidth, client.height);
		        }
		        int marginHeight = client.height - rect.height;
		        if (marginHeight > 0) {
		          gc.fillRectangle(0, rect.height, client.width,
		                  marginHeight);
		        }
			}
		});
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}
	
	protected void createLabels() {
		
		lblOffset = new Label(routePlanning, SWT.NONE);
		lblOffset.setBounds(509, 566, 37, 15);
		lblOffset.setText("Offset:");
		
		lblPathToFile = new Label(routePlanning, SWT.NONE);
		lblPathToFile.setBounds(10, 597, 169, 15);
		lblPathToFile.setText("Browse...");
		
		lblInitialState = new Label(routePlanning, SWT.NONE);
		lblInitialState.setBounds(10, 628, 85, 15);
		lblInitialState.setText("Initial state:");
	
		lblGoalState = new Label(routePlanning, SWT.NONE);
		lblGoalState.setBounds(10, 656, 64, 15);
		lblGoalState.setText("Goal state:");
	
		lblChooseAlgorithm = new Label(routePlanning, SWT.NONE);
		lblChooseAlgorithm.setBounds(10, 684, 64, 15);
		lblChooseAlgorithm.setText("Algorithm: ");
		
		lblInitX = new Label(routePlanning, SWT.NONE);
		lblInitX.setBounds(114, 628, 16, 15);
		lblInitX.setText("X:");
		
		lblInitY = new Label(routePlanning, SWT.NONE);
		lblInitY.setBounds(195, 628, 16, 15);
		lblInitY.setText("Y:");
		
		lblGoalX = new Label(routePlanning, SWT.NONE);
		lblGoalX.setBounds(114, 656, 16, 15);
		lblGoalX.setText("X:");
		
		lblGoalY = new Label(routePlanning, SWT.NONE);
		lblGoalY.setBounds(195, 656, 16, 15);
		lblGoalY.setText("Y:");
		
		lblBrowseImage = new Label(routePlanning, SWT.NONE);
		lblBrowseImage.setBounds(10, 567, 169, 15);
		lblBrowseImage.setText("Browse image...");
		
		lblCoordinatesScale = new Label(routePlanning, SWT.NONE);
		lblCoordinatesScale.setBounds(660, 566, 35, 15);
		lblCoordinatesScale.setText("Scale:");
		
		txtOutput = new Text(routePlanning, SWT.BORDER | SWT.READ_ONLY | SWT.V_SCROLL | SWT.MULTI);
		txtOutput.setBounds(266, 588, 480, 116);
		
	}

	protected void createButtons() {
		
		btnLoadImage = new Button(routePlanning, SWT.NONE);
		btnLoadImage.setBounds(185, 561, 75, 25);
		btnLoadImage.setText("Load Image");
		btnLoadImage.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				FileDialog fd = new FileDialog(routePlanning.getShell(), SWT.NULL);
				fd.setFilterExtensions(new String[] {"*.jpg"});
				String path = fd.open();
				if(path != null && path.length() > 0) {
					lblBrowseImage.setText(path);
					origImage = new Image(Display.getDefault(), path);
					mapImage = new Image(Display.getDefault(), origImage.getImageData());
					
					Rectangle rect = mapImage.getBounds();
			        Rectangle client = canvas.getClientArea();
			        hBar.setMaximum(rect.width);
			        vBar.setMaximum(rect.height);
			        hBar.setThumb(Math.min(rect.width, client.width));
			        vBar.setThumb(Math.min(rect.height, client.height));
			        int hPage = rect.width - client.width;
			        int vPage = rect.height - client.height;
			        int hSelection = hBar.getSelection();
			        int vSelection = vBar.getSelection();
			        if (hSelection >= hPage) {
			          if (hPage <= 0)
			            hSelection = 0;
			          origin.x = -hSelection;
			        }
			        if (vSelection >= vPage) {
			          if (vPage <= 0)
			            vSelection = 0;
			          origin.y = -vSelection;
			        }
			        solveIt();
				}
			}
		});
		
		btnLoadData = new Button(routePlanning, SWT.NONE);
		btnLoadData.setBounds(185, 592, 75, 25);
		btnLoadData.setText("Load Data");
		btnLoadData.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				FileDialog fd = new FileDialog(routePlanning.getShell(), SWT.NULL);
				fd.setFilterExtensions(new String[] {"*.txt"});
				String path = fd.open();
				if(path == null) return;
				if (!heu.loadMapProblem(path)) {
					alert.setText("Invalid input file!");
					alert.setMessage(path + " is an invalid input file!");
					alert.open();
				} else {
					lblPathToFile.setText(path);
					solveIt();
				}
			}
		});
		
		combo = new Combo(routePlanning, SWT.READ_ONLY);
		combo.setItems(new String[] {"Breadth First", "Iterative Deepening", "Uniform Cost", "Greedy Best First", "A*", "RBFS"});
		combo.setBounds(80, 681, 180, 23);
		
		btnReverseXY = new Button(routePlanning, SWT.CHECK);
		btnReverseXY.setBounds(266, 566, 85, 16);
		btnReverseXY.setText("Reverse X/Y");
		btnReverseXY.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				reverseXY = btnReverseXY.getSelection();
				solveIt();
			}
		});
		
		btnMirrorX = new Button(routePlanning, SWT.CHECK);
		btnMirrorX.setBounds(357, 566, 64, 16);
		btnMirrorX.setText("Mirror X");
		btnMirrorX.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				mirrorX = btnMirrorX.getSelection();
				solveIt();
			}
		});
		
		btnMirrorY = new Button(routePlanning, SWT.CHECK);
		btnMirrorY.setBounds(427, 566, 64, 16);
		btnMirrorY.setText("Mirror Y");
		btnMirrorY.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				mirrorY = btnMirrorY.getSelection();
				solveIt();
			}
		});
		
		combo.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent arg0) {
				algorithm = combo.getSelectionIndex();
				solveIt();
			}
		});
	}
	
	protected void createSpinners() {
		spinnerInitY = new Spinner(routePlanning, SWT.BORDER);
		spinnerInitY.setMaximum(200);
		
		spinnerInitX = new Spinner(routePlanning, SWT.BORDER);
		spinnerInitX.setMaximum(200);
		
		spinnerGoalX = new Spinner(routePlanning, SWT.BORDER);
		spinnerGoalX.setMaximum(200);
		spinnerGoalX.setSelection(200);
		
		spinnerGoalY = new Spinner(routePlanning, SWT.BORDER);
		spinnerGoalY.setMaximum(200);
		spinnerGoalY.setSelection(200);
		
		spinnerInitX.setBounds(132, 625, 47, 22);
		spinnerInitX.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				initX = spinnerInitX.getSelection(); 
				heu.setInit(initX, initY);
				solveIt();
			}
		});
		
		spinnerInitY.setBounds(213, 625, 47, 22);
		spinnerInitY.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				initY = spinnerInitY.getSelection(); 
				heu.setInit(initX, initY);
				solveIt();
			}
		});
		
		spinnerGoalX.setBounds(132, 653, 47, 22);
		spinnerGoalX.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				goalX = spinnerGoalX.getSelection(); 
				heu.setGoal(goalX, goalY);
				solveIt();
			}
		});
		
		spinnerGoalY.setBounds(213, 653, 47, 22);
		spinnerGoalY.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				goalY = spinnerGoalY.getSelection(); 
				heu.setGoal(goalX, goalY);
				solveIt();
			}
		});
		
		scaleSpinner = new Spinner(routePlanning, SWT.BORDER);
		scaleSpinner.setMinimum(1);
		scaleSpinner.setSelection(10);
		scaleSpinner.setBounds(699, 560, 47, 22);
		scaleSpinner.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				coordScale = scaleSpinner.getSelection();
				solveIt();
			}
		});
		
		spinnerOffsetY = new Spinner(routePlanning, SWT.BORDER);
		spinnerOffsetY.setMinimum(-100);
		spinnerOffsetY.setBounds(605, 560, 47, 22);
		spinnerOffsetY.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				yOffset = spinnerOffsetY.getSelection();
				solveIt();
			}
		});
		
		spinnerOffsetX = new Spinner(routePlanning, SWT.BORDER);
		spinnerOffsetX.setMinimum(-100);
		spinnerOffsetX.setBounds(552, 560, 47, 22);
		spinnerOffsetX.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				xOffset = spinnerOffsetX.getSelection();
				solveIt();
			}
		});
	}
	
	protected void solveIt() {
		disableAll();
		trd = new Thread() {
			HeurekaMap p = heu;
			public void run() {
	    		long ms = System.currentTimeMillis();
	        	p.solve(algorithm);
	     		ms = System.currentTimeMillis() - ms;
     		
	     		String eol = System.getProperty("line.separator");
	     		final String text = "Time for execution: " + Long.toString(ms) + " ms" + eol + eol +
	     							"Explored states: " + Long.toString(Search.exploredStates) + eol +
	     							"Generated nodes: " + Long.toString(Search.generatedNodes) + eol + eol +
	     							"Solution action sequence: " + eol + p.getSolution() + eol + eol +
	     							"Expanding sequence (debug): " + eol + Search.searchTrace + eol;
	     		
	     		Display.getDefault().syncExec(new Runnable() {
	     				public void run() {
	     					txtOutput.setText(text);
	     					drawGraph();
	     					enableAll();
	     				}
	     		});
			}
		};
		trd.start();
	}
	
	protected void enableAll() {
		txtOutput.setEnabled(true);
		
		scaleSpinner.setEnabled(true);
		spinnerInitY.setEnabled(true);
		spinnerInitX.setEnabled(true);
		spinnerGoalX.setEnabled(true);
		spinnerGoalY.setEnabled(true);
		spinnerOffsetY.setEnabled(true);
		spinnerOffsetX.setEnabled(true);
		
		combo.setEnabled(true);
		btnLoadData.setEnabled(true);
		btnLoadImage.setEnabled(true);
		btnReverseXY.setEnabled(true);
		btnMirrorX.setEnabled(true);
		btnMirrorY.setEnabled(true);

		canvas.setEnabled(true);
		
		if(focused != null) focused.setFocus();
	}
	
	protected void disableAll() {
		focused = routePlanning.getDisplay().getFocusControl();
		
		txtOutput.setEnabled(false);
		
		scaleSpinner.setEnabled(false);
		spinnerInitY.setEnabled(false);
		spinnerInitX.setEnabled(false);
		spinnerGoalX.setEnabled(false);
		spinnerGoalY.setEnabled(false);
		spinnerOffsetY.setEnabled(false);
		spinnerOffsetX.setEnabled(false);
		
		combo.setEnabled(false);
		btnLoadData.setEnabled(false);
		btnLoadImage.setEnabled(false);
		btnReverseXY.setEnabled(false);
		btnMirrorX.setEnabled(false);
		btnMirrorY.setEnabled(false);
		
		canvas.setEnabled(false);
	}

	protected void drawGraph() {
		mapImage = new Image(Display.getDefault(), origImage.getBounds().width, origImage.getBounds().height);
		GC gc = new GC(mapImage);
		gc.drawImage(origImage, 0, 0);
		
		gc.setLineWidth(5);
		gc.setForeground(new Color(Display.getDefault(), 0, 0, 255));
		gc.setBackground(new Color(Display.getDefault(), 0, 0, 255));
		
		LinkedList<Rectangle> edges = heu.getEdges();
		LinkedList<Rectangle> solution = heu.getSolutionEdges();
		LinkedList<Point> nodes = heu.getNodes();
		Iterator<Rectangle> eit = edges.iterator();
		Iterator<Rectangle> sit = solution.iterator();
		Iterator<Point> nit = nodes.iterator();
		
		Rectangle rect;
		Point pnt;
		Point pnt2;
		while(eit.hasNext()) {
			rect = eit.next();
			pnt = translateCoordinates(rect.x, rect.y);
			pnt2 = translateCoordinates(rect.width, rect.height);
			gc.drawLine(pnt.x, pnt.y, pnt2.x, pnt2.y);
		}
		
		while(nit.hasNext()) {
			pnt = nit.next();
			pnt = translateCoordinates(pnt.x, pnt.y);
			gc.fillOval(pnt.x - 10, pnt.y - 10, 20, 20);
		}
		
		Point init = heu.getInit();
		Point goal = heu.getGoal();
		
		gc.setLineWidth(3);
		gc.setForeground(new Color(Display.getDefault(), 0, 255, 0));
		while(sit.hasNext()) {
			rect = sit.next();
			pnt = translateCoordinates(rect.x, rect.y);
			pnt2 = translateCoordinates(rect.width, rect.height);
			gc.drawLine(pnt.x, pnt.y, pnt2.x, pnt2.y);
		}
		
		gc.setForeground(new Color(Display.getDefault(), 255, 0, 0));
		gc.setBackground(new Color(Display.getDefault(), 255, 0, 0));
		if(init != null) {
			pnt = translateCoordinates(init.x, init.y);
			gc.fillOval(pnt.x - 5, pnt.y - 5, 10, 10);
			
			pnt = translateCoordinates(initX, initY);
			gc.drawLine(pnt.x - 5, pnt.y, pnt.x + 5, pnt.y);
			gc.drawLine(pnt.x, pnt.y - 5, pnt.x, pnt.y + 5);
		}
		
		gc.setForeground(new Color(Display.getDefault(), 0, 255, 0));
		gc.setBackground(new Color(Display.getDefault(), 0, 255, 0));
		if(goal != null) {
			pnt = translateCoordinates(goal.x, goal.y);
			gc.fillOval(pnt.x - 5, pnt.y - 5, 10, 10);
			
			pnt = translateCoordinates(goalX, goalY);
			gc.drawLine(pnt.x - 5, pnt.y, pnt.x + 5, pnt.y);
			gc.drawLine(pnt.x, pnt.y - 5, pnt.x, pnt.y + 5);
		}
		
		gc.dispose();
		canvas.redraw(); 
	}
	
	protected Point translateCoordinates(int x, int y) {
		Point pnt = new Point(0, 0);
		pnt.x = coordScale * (reverseXY ? y : x) + xOffset;
		pnt.x = mirrorX ? origImage.getBounds().width - pnt.x : pnt.x;
		pnt.y = coordScale * (reverseXY ? x : y) + yOffset;
		pnt.y = mirrorY ? origImage.getBounds().height - pnt.y : pnt.y;
		return pnt;
	}
	
	protected Point retranslateCoordinates(int x, int y) {
		Point pnt = new Point(0, 0);
		
		pnt.x = (mirrorX ? origImage.getBounds().width - x : x) - xOffset;
		pnt.y = (mirrorY ? origImage.getBounds().height - y : y) - yOffset;
		
		int xx = (int) Math.round((double) (reverseXY ? pnt.y : pnt.x) / coordScale);
		int yy = (int) Math.round((double) (reverseXY ? pnt.x : pnt.y) / coordScale);
		
		pnt.x = xx;
		pnt.y = yy;

		return pnt;
	}

}
